import React from 'react';
import 'react-native-gesture-handler';
import {NativeBaseProvider} from "native-base";
import {NavigationContainer} from '@react-navigation/native';

import Routes from './routes';
import {AuthProvider} from './contexts/auth';
import {theme} from "./components/theme";

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <AuthProvider>
        <NativeBaseProvider theme={theme}>
            <Routes />
        </NativeBaseProvider>
      </AuthProvider>
    </NavigationContainer>
  );
};

export default App;
