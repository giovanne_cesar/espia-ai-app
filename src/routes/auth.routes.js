import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import SignIn from '../pages/SignIn';
import ChooseSignIn from "../pages/ChooseSignIn";

const AuthStack = createNativeStackNavigator();

export const AuthRoutes = () => (
  <AuthStack.Navigator screenOptions={{
    headerShown: false
  }}>
    <AuthStack.Screen name={'Login'} component={SignIn} />
  </AuthStack.Navigator>
);

export const ChooseUserRoutes = () => (
  <AuthStack.Navigator screenOptions={{
    headerShown: false
  }}>
    <AuthStack.Screen name={'Choose User'} component={ChooseSignIn} />
  </AuthStack.Navigator>
);




