import React from "react";
import { Text, View, VStack, Link, Box, Divider } from "native-base";
import api from "../../../services/api";
//Todo: Front
const TaskScreen = ({route}) => {
  const baseUrl = api.defaults.baseURL+"/"
  const task = route.params.ContentProps
    return(
      <View bg={'#f0f2f5'}>
        <Box padding={5} bg={'white'} rounded={30} margin={1}>
          <Text fontSize={'2xl'} bold> {task.name}</Text>
          <Divider />
          <Text fontSize={'xl'} > {task.description}</Text>
          <Link href={baseUrl+task.attachment} marginTop={2}>
            <Box px="3" py="2" bg="red.500" rounded="sm" _text={{
              color: "white",
              fontWeight: "medium"
            }}>
              PDF
            </Box>
          </Link>
        </Box>
      </View>

  )

}

export default TaskScreen
