import React from 'react';
import { Button, VStack, View, Text, HStack, TextArea, Pressable } from "native-base";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import DocumentPicker from 'react-native-document-picker';
import ProfilePicture from "../../components/ProfilePicture";
import { useAuth } from "../../contexts/auth";
import { makePost } from "../../services/feed";
import { useNavigation } from "@react-navigation/native";

//Todo: Não ta fazendo o post na api
const MakePost: React.FC = ({navigation}) => {
  const navigaton = useNavigation()
  const user = useAuth().user
  const [description, setDescription] = React.useState('')
  const [image, setImage] = React.useState('')

  function createPost(){
    navigation.navigate('FeedBarBottom')
  }
  const selectImage = async () => {
// Opening Document Picker to select one file
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      //console.log('res : ' + JSON.stringify(res));
      // Setting the state to show single file attributes
      setImage(res[0]);
    } catch (err) {
      setImage(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        alert('Canceled');
      } else {
        // For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }

  return (
    <View padding={5}>
      <VStack space={5}>
        <HStack space={2} alignItems={'center'}>
          <ProfilePicture image={user.photo} size={50}/>
          <Text fontSize={'xl'}>{user.name}</Text>
        </HStack>
        <TextArea h={40} placeholder={"Faça uma publicação"} w={'100%'} onChangeText={value => setDescription(value)}/>
        <VStack space={2}>
          <Text>Adicione também à sua publicação</Text>
          <Pressable>
            <HStack space={2} alignItems={'center'} >
              <FontAwesome5 name={'image'} size={15}/>
              <Button onPress={selectImage}>Imagem</Button>
            </HStack>
          </Pressable>
        </VStack>
        <Button onPress={createPost}>Publicar</Button>
      </VStack>
    </View>

  );
};

export default MakePost;
