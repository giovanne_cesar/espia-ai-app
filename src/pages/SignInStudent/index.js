import React from 'react';
import { Input, Center, Button, VStack, FormControl } from "native-base";
import EetepaLogo from '../../components/EetepaLogo';
import { getEmployee } from "../../services/auth";

import { useAuth } from "../../contexts/auth";

function SignInStudent({navigation}) {
  //const [show, setShow] = React.useState(false);
  //const handleClick = () => setShow(!show);
  const context = useAuth()

  const [invalid, setInvalid] = React.useState(false);
  const [registrationMessageErro, setRegistrationMessageError] = React.useState('');
  const [passwordMessageErro, setPasswordlMessageErro] = React.useState('');

  const [registration, setRegistration] = React.useState('');
  const [password, setPassword] = React.useState('');

  async function handleSignIn() {
    if(registration === ''){
      setInvalid(true);
      setRegistrationMessageError('Preencha o campo de Matrícula');
      if(password === ''){
        setPasswordlMessageErro('Preencha o campo senha');
      }
    }else{
      if(password === ''){
        setInvalid(true);
        setRegistrationMessageError('');
        setPasswordlMessageErro('Preencha o campo senha')
      }else{
        setInvalid(false);
        //const student = await getEmployee(6)
        const student = false
        //context.setUserAuth(student)
        if(student){
          navigation.navigate('FeedStudent');
        }
        else{
          setInvalid(true);
          setPasswordlMessageErro('Informações não encontradas')
        }
      }
    }

  }

  return (
    <Center flex={1}>
      <VStack space="20">
        <Center>
          <EetepaLogo />
        </Center>
        <VStack space={3}>
          <FormControl isRequired isInvalid={invalid}>
            <FormControl.Label>Matrícula do Estudante</FormControl.Label>
            <Input onChangeText={value => setRegistration(value)} />
            <FormControl.ErrorMessage>
              {registrationMessageErro}
            </FormControl.ErrorMessage>
            <FormControl.Label>Senha</FormControl.Label>
            <Input type={'password'} onChangeText={value => setPassword(value)} />
            <FormControl.ErrorMessage>
              {passwordMessageErro}
            </FormControl.ErrorMessage>
          </FormControl>
        </VStack>
        <VStack space={5}>
          <Button onPress={handleSignIn}>Logar</Button>
          <Button onPress={() => navigation.goBack()}>Voltar</Button>
        </VStack>
      </VStack>
    </Center>
  );
}

export default SignInStudent;
