import React, { useEffect } from "react";
import { Center, VStack, Text, Spinner, ScrollView } from "native-base";
import EetepaLogo from "../../components/EetepaLogo";
import { getInstitutionById, getInstitutions } from "../../services/auth";
import { NativeBaseProvider } from "native-base/src/core/NativeBaseProvider";
import School from "../../components/School";

const Institution: React.FC = ({navigation}) => {

  const [loading, setLoading] = React.useState(true)
  const [schools, setSchools] = React.useState([])

  useEffect(() => {
    async function attPage(){
      const institutions = await getInstitutions()
      const temp = []
      for(let i = 0 ; i < institutions.length; i++){
        const school = await getInstitutionById(institutions[i].id)
        temp.push(
          <School SchoolProps={school} navigation={navigation} key={school.id}/>
        )
      }
      setSchools(temp)
      setLoading(false)
    }
    attPage()
  }, [])

  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }
  return (
    <Center flex={1}>
      <VStack space={10} alignItems={'center'} marginTop={5}>
        <EetepaLogo />
        <Text fontSize={'2xl'}>Selecione uma EETEPA para obter informações</Text>
        <ScrollView>
          <VStack space={2} minW={'95%'}>
            {schools}
          </VStack>
        </ScrollView>
      </VStack>
    </Center>
  );
};

export default Institution;
