import React from 'react';
import { Button, VStack, View, Text, HStack, Pressable, Center } from "native-base";
import ToggleDrawer from "../../components/ToggleDrawer";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const Configuration: React.FC = ({navigation}) => {
  function signOut(){
    navigation.navigate('Choose User')
  }
  return (
    <Center flex={1}>
      <Button marginTop={10} onPress={signOut}>Deslogar</Button>
      <ToggleDrawer navigation={navigation}/>
    </Center>



  );
};

export default Configuration;

