import React from 'react';
import { Input, Stack, Center, Box, Button, Text, VStack, HStack, Divider, Flex } from "native-base";
import {useAuth} from '../../contexts/auth';
import EetepaLogo from "../../components/EetepaLogo";

const Dashboard: React.FC = ({navigation}) => {

  function handlenaosei(){
    console.log("nao sei");
  }

  return (
    <Center flex={1}>
      <VStack space={'20'} alignItems={'center'} justifyContent={'center'}>
        <EetepaLogo/>
        <HStack space={5}>
          <Stack
            direction={{
              base:'column',
              md:'row',
            }}
            space={'5'}>
            <Button size={"lg"} padding={'5'} onPress={() => navigation.navigate('Feed')}>Noticias</Button>
            <Button size={"lg"} padding={'5'} onPress={() => navigation.navigate('Institution')}>Instituição</Button>
          </Stack>
          <Stack
            direction={{
              base:'column',
              md:'row',
            }}
            space={'5'}>
            <Button size={"lg"} padding={'5'} onPress={() => navigation.navigate('Classes')}>Turmas</Button>
            <Button size={"lg"} padding={'5'} onPress={handlenaosei}>Configurações</Button>
          </Stack>
        </HStack>

        <Button onPress={() => navigation.goBack()}>Voltar</Button>
      </VStack>

    </Center>
  );
};

export default Dashboard;
