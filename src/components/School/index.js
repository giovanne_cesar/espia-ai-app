import React from "react";
import { Box, Pressable, Text, VStack } from "native-base";
import { setSchool } from "../../contexts/institution";

const School = ({SchoolProps, navigation}) => {

  function onEvent(){
    setSchool(SchoolProps)
    navigation.navigate('InstitutionPages')
  }

  return(
    <Pressable  margin={'3px'} onPress={onEvent}>
      <Box bg={'white'} padding={2} borderWidth={'2px'} rounded={'8'}  borderColor={'#dbdbdb'}>
        <VStack>
          <Text fontSize={'2xl'}>{SchoolProps.name}</Text>
          <Text color={'gray.400'} fontSize={'xs'}>{SchoolProps.public_place} nº {SchoolProps.public_place_number}</Text>
        </VStack>
      </Box>
    </Pressable>
  )
}

export default School
