import React from "react";
import { IconButton } from "native-base";
import Entypo from "react-native-vector-icons/Entypo";
//Todo refatorar

export default ({navigation}) => {

  function onPressButton() {
    navigation.toggleDrawer();
  }

  return(
    <IconButton
      icon={<Entypo name={'menu'} size={26}/>}
      borderRadius={"full"}
      position={'absolute'}
      top={5}
      right={5}
      bg={'red.200'}
      onPress={onPressButton}
    />
  );
};
