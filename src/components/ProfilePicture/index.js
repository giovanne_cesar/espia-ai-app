import React from "react";
import { Image } from "native-base";
import api from "../../services/api";
const baseUrl = api.defaults.baseURL+"/storage/"
const ProfilePicture = ({image, size= 50}) => {
  return(
    <Image
      source={{uri: baseUrl+image}}
      alt={'picture'}
      size={size}
      borderRadius={size}
    />
    )
}

export default ProfilePicture
