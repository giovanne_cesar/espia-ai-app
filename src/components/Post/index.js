import React from "react";
import { Box, View, VStack } from "native-base";
import TopContainer from "./TopContainer";
import BotContainer from "./BotContainer";


const Post = ({PostProps, EmployeeProps}) => {

  return(
    <Box bg={'white'} borderRadius={20} marginBottom={3}>
      <View padding={3}>
        <VStack>
          <TopContainer post={PostProps} employee={EmployeeProps}  />
          <BotContainer post={PostProps} />
        </VStack>
      </View>
    </Box>

  )
}

export default Post
