import React, { useEffect } from "react";
import { HStack, Image, Text, View, VStack } from "native-base";
import ProfilePicture from "../../ProfilePicture";

const TopContainer = ({post, employee}) => {
  const date = post.date.split('-')
  return(
      <HStack alignItems={'center'} space={2}>
        <ProfilePicture image={employee.photo}/>
        <VStack>
          <Text bold>{post.employee.name} - {employee.role.name}</Text>
          <HStack space={4}>
            <Text color={"gray.300"}>{date[2]}/{date[1]}/{date[0]}</Text>
            <Text color={"gray.300"}>{employee.school.name}</Text>
          </HStack>

        </VStack>
      </HStack>
  )
}

export default TopContainer
