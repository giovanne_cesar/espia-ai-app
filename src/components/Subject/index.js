import React from "react";
import { Box, Pressable, Text, VStack } from "native-base";
import { setSubjectClass } from "../../contexts/subjects";

const Subject = ({SubjectProps, navigation}) => {

  function onEvent(){
    setSubjectClass(SubjectProps)
    navigation.navigate('SubjectPages')
  }

  return(
    <Pressable margin={'5px'} onPress={onEvent} bg={'white'}>
      <Box borderWidth={'2px'} rounded={'8'}  borderColor={'#dbdbdb'}>
        <VStack space={2} >
          <Text fontSize={'2xl'} marginLeft={2}>Turma de {SubjectProps.subject.name}</Text>
          <Box padding={1} bg={'#dbdbdb'}>
            <Text fontSize={'sm'} marginLeft={2}>Professor: {SubjectProps.employee.name}</Text>
          </Box>
        </VStack>
      </Box>
    </Pressable>
  )
}

export default Subject
