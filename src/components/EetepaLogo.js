import React from 'react';
import {Image} from 'native-base';

export default () => {
  return (
    <Image
      source={require('../images/logoEetepa.png')}
      alt="Logo da Eetepa"
      resizeMode="contain"
    />
  );
};
